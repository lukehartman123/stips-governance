import time
from typing import Dict, Optional, List
from voting_methods import ranked_choice_voting
from flask import Flask, jsonify, render_template, request, redirect, url_for


presentation_owners: Dict[str, str] = {}

presentation_durations: Dict[str, float] = {}

admins = ["Luke", "Rishi"]

users = [
    "James",
    "Joe",
    "Josiah",
    "Harrison",
    "Karl 1",
    "Karl 2",
    "Kendrick",
    "Luke",
    "Mendel",
    "Phillip",
    "Rachel",
    "Reed",
    "Rishi",
    "Vaughn",
]

# username, presentation -> wtp per hour
# TODO Change to nested
unrankings: Dict[str, List[str]] = {
    username: list(presentation_owners)
    for username in users
}
rankings: Dict[str, List[str]] = {
    username: []
    for username in users
}


is_ready: Dict[str, bool] = {
    username: False
    for username in users
}
is_included: Dict[str, bool] = {
    username: False
    for username in users
}
current_presentation: Optional[str] = None
presentation_end: Optional[int] = None
previous_presentations: List[str] = []  # Includes current_presentation


def verify(info: Dict, is_admin=False) -> bool:
    if is_admin:
        return info["username"] in admins
    return info["username"] in users


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        info = request.form
        if verify(info):
            return redirect(url_for('user', username=info["username"]))
        return render_template('login.html', default=info["username"], failed=True)
    if request.method == 'GET':
        return render_template('login.html', default="", failed=False)


@app.route('/user/<username>', methods=['GET'])
def user(username):
    if username in users:
        return render_template(
            'user.html',
            username=username,
            is_admin=username in admins,
            presentation_durations=presentation_durations,
            presentation_owners=presentation_owners,
            ready=is_ready[username],
            current_presentation=current_presentation,
            presentation_end=presentation_end,
            previous_presentations=previous_presentations,
            presentation_hashes={presentation: hash(presentation) for presentation in presentation_durations},
            ranked=rankings[username],
            unranked=unrankings[username],
        )
    return redirect(url_for('login'))


@app.route('/admin/<username>', methods=['GET'])
def admin(username):
    if username in admins:
        return render_template(
            'admin.html',
            username=username,
            is_ready=is_ready,
            is_included=is_included,
            current_presentation=current_presentation,
        )
    return redirect(url_for('login'))


@app.route('/submit_presentation', methods=['POST'])
def submit_presentation():
    info = request.form
    try:
        if verify(info) and info["presentation name"] not in presentation_owners:
            presentation_owners[info["presentation name"]] = info["username"]
            presentation_durations[info["presentation name"]] = int(info["presentation duration"])
            for username in users:
                unrankings[username].append(info["presentation name"])
            return redirect(url_for('user', username=info["username"]))
    except ValueError:
        pass
    return jsonify(
        "You done goofed. I'll fix this later."
    )


@app.route('/delete_presentation', methods=["POST"])
def delete_presentation():
    info = request.form
    if verify(info) and info["presentation name"] in presentation_owners:
        del presentation_owners[info["presentation name"]]
        del presentation_durations[info["presentation name"]]
        global rankings
        rankings = {
            username: [
                presentation for presentation in rankings[username]
                if presentation != info["presentation name"]
            ]
            for username in users
        }
        global unrankings
        unrankings = {
            username: [
                presentation for presentation in unrankings[username]
                if presentation != info["presentation name"]
            ]
            for username in users
        }
        return redirect(url_for('user', username=info["username"]))
    return jsonify(
        "You done goofed. I'll fix this later."
    )

@app.route('/update_rankings', methods=['POST'])
def update_rankings():
    info = request.json
    if verify(info):
        def undo_hash(item: int) -> str:
            for presentation in presentation_owners:
                if hash(presentation) == item:
                    return presentation
            assert False
        unrankings[info["username"]] = [undo_hash(int(item)) for item in info["unranked"]]
        rankings[info["username"]] = [undo_hash(int(item)) for item in info["ranked"]]
        # remove out of date presentations from unrankings + rankings
        rankings[info["username"]] = [
            presentation for presentation in rankings[info["username"]]
            if presentation not in previous_presentations
        ]
        unrankings[info["username"]] = [
            presentation for presentation in unrankings[info["username"]]
            if presentation not in previous_presentations
        ]
        # add any missed presentations to unrankings
        for presentation in presentation_owners:
            if presentation in previous_presentations:
                continue
            if presentation in rankings[info["username"]]:
                continue
            if presentation in unrankings[info["username"]]:
                continue
            unrankings[info["username"]].append(presentation)
    return jsonify(
        "You done goofed. I'll fix this later."
    )

@app.route('/select_presentation', methods=['POST'])
def select_presentation():
    info = request.form
    if verify(info, is_admin=True):
        usernames = [username for username, included in is_included.items() if included]
        presentations = [presentation for presentation, owner in presentation_owners.items() if owner in usernames and presentation not in previous_presentations]

        global rankings
        winning_presentation = ranked_choice_voting(usernames, presentations, rankings)
        rankings = {
            username: [
                presentation for presentation in rankings[username]
                if presentation != winning_presentation
            ]
            for username in users
        }
        global unrankings
        unrankings = {
            username: [
                presentation for presentation in unrankings[username]
                if presentation != winning_presentation
            ]
            for username in users
        }
        for username in usernames:
            is_ready[username] = False
        global current_presentation
        current_presentation = winning_presentation
        global presentation_end
        presentation_end = int(time.time() + 60*presentation_durations[winning_presentation])
        previous_presentations.append(current_presentation)
        return redirect(url_for('user', username=info["username"]))
    return jsonify(
        "You done goofed. I'll fix this later."
    )


@app.route('/create_user', methods=['POST'])
def create_user():
    info = request.form
    if verify(info, is_admin=True):
        users.append(info["username to modify"])
        is_ready[info["username to modify"]] = False
        unrankings[info["username to modify"]] = list(presentation_owners)
        return redirect(url_for('admin', username=info["username"]))
    return jsonify(
        "You done goofed. I'll fix this later."
    )


@app.route('/delete_user', methods=['POST'])
def delete_user():
    info = request.form
    if verify(info, is_admin=True):
        users.remove(info["username to modify"])
        if info["username to modify"] in admins:
            admins.remove(info["username to modify"])
        del is_ready[info["username to modify"]]
        del unrankings[info["username to modify"]]
        del rankings[info["username to modify"]]
        return redirect(url_for('admin', username=info["username"]))
    return jsonify(
        "You done goofed. I'll fix this later."
    )


@app.route('/set_ready', methods=['POST'])
def set_ready():
    info = request.form
    if verify(info):
        is_ready[info["username"]] = bool(int(info["ready"]))
        return redirect(url_for('user', username=info["username"]))
    return jsonify(
        "You done goofed. I'll fix this later."
    )


@app.route('/set_included', methods=['POST'])
def set_included():
    info = request.form
    if verify(info, is_admin=True):
        is_included[info["username to modify"]] = "included" in info
        return redirect(url_for('admin', username=info["username"]))
    return jsonify(
        "You done goofed. I'll fix this later."
    )

if __name__ == "__main__":
    app.run()
