import random

from typing import Dict, List
from collections import Counter


def ranked_choice_voting(usernames: List[str], presentations: List[str], rankings: Dict[str, List[str]]) -> str:
    all_rankings = []
    shuffled_presentations = presentations.copy()
    random.shuffle(shuffled_presentations)

    for username in usernames:
        all_rankings.append(
            (
                rankings[username] + [
                    presentation for presentation in shuffled_presentations
                    if presentation not in rankings[username]
                ]
            )
        )
    winning_threshold = len(usernames) // 2
    while True:
        counter = Counter([users_rankings[0] for users_rankings in all_rankings])
        sorted_by_first_place_frequency = counter.most_common()
        most_popular, most_popular_votes = sorted_by_first_place_frequency[0]
        if most_popular_votes >= winning_threshold:
            return most_popular
        least_popular = sorted_by_first_place_frequency[0][0]
        for users_rankings in all_rankings:
            users_rankings.remove(least_popular)


if __name__ == "__main__":
    usernames = ["A", "B", "C"]
    presentations = ["1", "2", "3"]
    rankings = {
        "A": ["1", "2", "3"],
        "B": ["2"],
        "C": ["2"],
    }
    print(ranked_choice_voting(usernames, presentations, rankings))