# STIPS Governance

Project for running meetings and making decisions for the Special Topics in Philosophy and Science group.

## Installation

Install the required packages. You may want to do this inside of a virtual environment.

If you want to install a virtual environment
```
pip install virtualenv
virtualenv venv
source venv/bin/activate
```

```
pip install -r requirements.txt
```

Download a couenne solver from https://ampl.com/dl/open/couenne/.

Add the solver to your PATH environment variable ([instructions](https://lmgtfy.app/#gsc.tab=0&gsc.q=adding%20a%20location%20to%20your%20path))